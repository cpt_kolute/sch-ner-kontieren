// ==UserScript==
// @name Schöner Kontieren
// @version 0.6
// @description improves usability of Communardo Zeiterfassung (hopefully)
// @grant none
// @unwrap
// @include /https://www\.communardo\.de/intranet/http/index.php\?.*site=time_recording-worktimerecording.*/
// @include /https://www\.communardo\.de/intranet/http/index.php\?.*site=time_recording-timerecweek.*/
// @include file:///C:/Programming/Zeiterfassung/Communardo%20Zeiterfassung.htm
// @namespace https://www.communardo.de/zeiterfassung
// @require http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// ==/UserScript==


jQuery.noConflict();
jQuery(document).ready(
  function($) {
    // some constants
    var KEYCODE_ESC = 27;
    var KEYCODE_F = 70;
    var KEYCODE_ENTER = 13;
    var FOLDED_COOKIE_NAME = "time_recording_folded";
    var FILTER_INPUT_ID =  "twiFilterInput";
    var FILTER_INPUT_SELECTOR = "#"+FILTER_INPUT_ID;
    var MESSAGE_DIV_ID = "twiWarning";
    var HIDDEN_CLASS = "hidden";
    var HIGHLIGHT_CLASS = "ccHighlight";
    
    // day or week recording    
    var TABLE_SELECTOR;
    var IS_WEEK_RECORDING;
    if ($("table#timerec").length > 0){
        TABLE_SELECTOR = "#timerec";
        IS_WEEK_RECORDING = false;
    } else if ($("table#timerecweek").length > 0){
        TABLE_SELECTOR = "#timerecweek";
        IS_WEEK_RECORDING = true;
    } else {
        // initialisation failed
        TABLE_SELECTOR = "#noSuchIdGrfzl";
        IS_WEEK_RECORDING = false;
        // TODO: do something meaningful
    }
    var ALL_COST_CENTER_SELECTOR = TABLE_SELECTOR + " tr.pathcc, " + TABLE_SELECTOR + " tr.cc";
  
    /*
     * jQuery Highlight plugin
     *
     * Based on highlight v3 by Johann Burkard
     * http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html
     *
     * Code a little bit refactored and cleaned (in my humble opinion).
     * Most important changes:
     *  - has an option to highlight only entire words (wordsOnly - false by default),
     *  - has an option to be case sensitive (caseSensitive - false by default)
     *  - highlight element tag and class names can be specified in options
     *
     * Usage:
     *   // wrap every occurrance of text 'lorem' in content
     *   // with <span class='highlight'> (default options)
     *   $('#content').highlight('lorem');
     *
     *   // search for and highlight more terms at once
     *   // so you can save some time on traversing DOM
     *   $('#content').highlight(['lorem', 'ipsum']);
     *   $('#content').highlight('lorem ipsum');
     *
     *   // search only for entire word 'lorem'
     *   $('#content').highlight('lorem', { wordsOnly: true });
     *
     *   // don't ignore case during search of term 'lorem'
     *   $('#content').highlight('lorem', { caseSensitive: true });
     *
     *   // wrap every occurrance of term 'ipsum' in content
     *   // with <em class='important'>
     *   $('#content').highlight('ipsum', { element: 'em', className: 'important' });
     *
     *   // remove default highlight
     *   $('#content').unhighlight();
     *
     *   // remove custom highlight
     *   $('#content').unhighlight({ element: 'em', className: 'important' });
     *
     *
     * Copyright (c) 2009 Bartek Szopka
     *
     * Licensed under MIT license.
     *
     */

    jQuery.extend({
        highlight: function (node, re, nodeName, className) {
            if (node.nodeType === 3) {
                var match = node.data.match(re);
                if (match) {
                    var highlight = document.createElement(nodeName || 'span');
                    highlight.className = className || 'highlight';
                    var wordNode = node.splitText(match.index);
                    wordNode.splitText(match[0].length);
                    var wordClone = wordNode.cloneNode(true);
                    highlight.appendChild(wordClone);
                    wordNode.parentNode.replaceChild(highlight, wordNode);
                    return 1; //skip added node in parent
                }
            } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
                    !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
                    !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
                for (var i = 0; i < node.childNodes.length; i++) {
                    i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
                }
            }
            return 0;
        }
    });

    jQuery.fn.unhighlight = function (options) {
        var settings = { className: 'highlight', element: 'span' };
        jQuery.extend(settings, options);

        return this.find(settings.element + "." + settings.className).each(function () {
            var parent = this.parentNode;
            parent.replaceChild(this.firstChild, this);
            parent.normalize();
        }).end();
    };

    jQuery.fn.highlight = function (words, options) {
        var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false };
        jQuery.extend(settings, options);
        
        if (words.constructor === String) {
            words = [words];
        }
        words = jQuery.grep(words, function(word, i){
          return word != '';
        });
        words = jQuery.map(words, function(word, i) {
          return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
        });
        if (words.length == 0) { return this; };

        var flag = settings.caseSensitive ? "" : "i";
        var pattern = "(" + words.join("|") + ")";
        if (settings.wordsOnly) {
            pattern = "\\b" + pattern + "\\b";
        }
        var re = new RegExp(pattern, flag);
        
        return this.each(function () {
            jQuery.highlight(this, re, settings.element, settings.className);
        });
    }
    
    // -------- End of jQuery highlight plugin -------
    
    // -------- Markup producing functions --------------
    
    /**
     *
     * Creates a new jQuery object representing the DOM element described by the given parameters:
        name of the element
        the innerElements (jQuery objects) (may be omitted)
        the attributes (PlainObject w/ key value pairs) (may be omitted)
     */
    function htmlElement(name, attributes, innerElements) {
        var domElement = $('<' +name+ '/>');
        if (attributes) {
            domElement.attr(attributes);
        }
        
        if (innerElements) {
            innerElements.appendTo(domElement);
        }
        
        return domElement;
    }
    
    function div(attributes, innerElements) {
        return htmlElement('div', attributes, innerElements);
    }
    
    function input(type, additionalAttributes) {
        var attributes = additionalAttributes;
        attributes.type = type;
        return htmlElement('input', attributes);
    }
    
    /**
     * Creates a text node.
     */
    function text(txt) {
        return $(document.createTextNode(txt))
    }
    
    
    
    function insertSearchBar() {
        var searchBar = div({style: "background-color: #CCDAE5; border: 1px solid #A8B8C7; position: fixed; top: 0; left: 0; z-index: 1000001;"},
                             input("text", {id: FILTER_INPUT_ID,
                                           style: "width: 130px; margin: 5px 15px; padding:3px; border: 1px solid #A8B8C7;"}));
        
        searchBar.prependTo(TABLE_SELECTOR + " tbody");
        $(FILTER_INPUT_SELECTOR, searchBar).focus();
    }
    
    
    
    function displayMessage(message) {
        
        var initKeyFrame = function(){
        
            // check if already initialized
            if ($('#message-keyframe').size() > 0){
                return;
            }
            
            var css = "@keyframes animateColors {  0% {    background-color: #CCDAE5;  }  100% {    background-color: red;  }}; ";
            
            $("<style>" + css + "</style>").attr({
                "class": "keyframe-style",
                id: "message-keyframe",
                type: "text/css"
            }).appendTo("head");
        }
        
        
        initKeyFrame();
        
        
        var msgContainer = div({id: MESSAGE_DIV_ID,
                                style: "font-size: 110%; width: 250px; margin-left: -125px; padding: 10px ; background-color: #CCDAE5; " +
                                       "border: 1px solid #A8B8C7; position: fixed; top: 0; left: 50%; z-index: 1000001; " +
                                       "animation: animateColors 1s linear 0s infinite alternate forwards running; "},
                               message);
        msgContainer.appendTo("body");
    }
    
    // -------- Event handlers --------------
    function focusSearchBar(event) {
        if (event.ctrlKey && event.keyCode == KEYCODE_F) {
            $(FILTER_INPUT_SELECTOR).focus();
            $(FILTER_INPUT_SELECTOR).select();
            return false;
        }
    }
    
    function preventSubmit(event) {
        if(event.which == KEYCODE_ENTER) {
            return false;
        }
    }
    
    
    /**
     * returns a function which starts filtering at 300 ms after beeing invoked
     * (lest not beeing invoked during that timespan, in which case it will wait for another 300 ms and so on)
     */
    function getFilterInvoker() {
        var filterTimer = 0;
        return function(event){
            clearTimeout(filterTimer);
            filterTimer = setTimeout(function() {
                    filterCostCenters(event)
                }, 300);
        }
    }

    function filterCostCenters(event){
        var phrase = $(FILTER_INPUT_SELECTOR).val();
        
        var allCostCenters = function(){
            return function(){
                return $(ALL_COST_CENTER_SELECTOR);
            }
        }();
        
        function showCostCenters(costCenterElems){
            costCenterElems.removeClass(HIDDEN_CLASS);
        }

        function hideCostCenters(costCenterElems){
            costCenterElems.addClass(HIDDEN_CLASS);
        }

        /**
         * Returns an array containing all descendants of the given element
         */
        function getDescendants(ccElem){
            // descendants are the children of ccElem and their thildren and so on until there are no further children
            // children are those elements that contain a class that is called like the id of their parent
            
            var descendants = $('.' + ccElem.attr("id"));
            descendants.each(function(idx, elem){
                                   descendants = descendants.add(getDescendants($(elem)));
                               });
            return descendants;
        }
        
        /**
         * Returns an array containing all ancestors of the given element
         */
        function getAncestors(ccElem){
            // ancestors are the parents of ccElem and their parents and so on until there is no further parent
            // a parent is an element that has an id that equals the class of ccElem starting with "cc"
            
            var ancestors = $([]);
            var ccIdMatch = ccElem.attr("class").match(/cc\d+/);
            if (ccIdMatch != null){
                var ccIdClass = ccIdMatch[0];
                var parent = $('#' + ccIdClass);
                ancestors = getAncestors(parent).add(parent);
            }
            return ancestors;
        }
        
        function showCostCenterTree(ccElemInTree){
            showCostCenters(ccElemInTree);
            showCostCenters(getDescendants(ccElemInTree));
            showCostCenters(getAncestors(ccElemInTree));
        }

        function hideAllCostCenters(){
            hideCostCenters(allCostCenters());
        }
        
        function showAllCostCenters(){
            showCostCenters(allCostCenters());
        }

        function getCostCenterElementsByNames(names){
            return $(TABLE_SELECTOR + " tr").filter(function(idx){
                var hits = names.map(function(name, nameIdx, array){
                    return $(this).text().toUpperCase().indexOf(name.toUpperCase()) >= 0;
                }, this);
                
                return hits.reduce(function(prev, curr, currIdx, array){
                    return prev && curr;
                }, true);
            });
        }
        
        function unHighlightAll(){
            allCostCenters().unhighlight({className:HIGHLIGHT_CLASS});
        }
        
        function highlightTerms(elem, terms){
            elem.highlight(terms, {className:HIGHLIGHT_CLASS});
            $("."+HIGHLIGHT_CLASS).css({backgroundColor: "#FFFF88"});
        }
        
        // returns the vale of the cookie with the given name or null if there is no such cookie
        function getCookie(cookieName){
            var valueMatch = new RegExp(cookieName + "=([^;]+)").exec(document.cookie);
            return valueMatch ? valueMatch[1] : null;
        }
        
        function resetToDefault(){
            unHighlightAll();
            showAllCostCenters();
            var flagFolded = getCookie(FOLDED_COOKIE_NAME);
            if(flagFolded != null && flagFolded == 1) {
                // trigger original event
                $("#switch-open").click();
            } else {
                // trigger original event
                $("#switch-close").click();
            }
        }
        
        // extracts separate terms from a given phrase
        function getTerms(phrase){
            return phrase.split(" ").filter(function(term, termIdx, termArray){
                return term.length > 2
            });
        }               
        
        if (event.which  == KEYCODE_ESC){
            $(FILTER_INPUT_SELECTOR).blur();
            $(FILTER_INPUT_SELECTOR).val("");
            resetToDefault();
        } else if (phrase.length > 2){
            hideAllCostCenters();
            unHighlightAll();
            getCostCenterElementsByNames([phrase]).each(function(id, ccElem){
                showCostCenterTree($(ccElem));
                highlightTerms($(ccElem), phrase);
            });
            var terms = getTerms(phrase);
            if (terms.length > 0){
                getCostCenterElementsByNames(terms).each(function(id, ccElem){
                    showCostCenterTree($(ccElem));
                    highlightTerms($(ccElem), terms);
                });
            }
        } else {
            resetToDefault();
        }
    }
    
    // ---------- detect booking on wrong day -------
    
    function checkDate(){
        
        // nothing to do if date warning already displayed
        if (jQuery('#' + MESSAGE_DIV_ID).size() > 0){
            return;
        }
    
        var getDateString = function(date) {
            var day = date.getDate();
            var month = (date.getMonth() + 1);
            
            return (day < 10 ? "0":"") + day + "." + (month < 10 ? "0":"") + month + "." + date.getFullYear();
        }
        
        var getLastMonday = function (date){
            var daysSinceMonday = date.getDay() - 1;
            if (daysSinceMonday == -1){ // Sunday is 0
                daysSinceMonday = 6;
            }
            var milliOffset = daysSinceMonday * 24 * 60 * 60 * 1000;
            return new Date(date.getTime() - milliOffset);
        }        
        
        if (IS_WEEK_RECORDING){
            var convertDate = function (date) {
                return getLastMonday(date);
            }
            var generateWarning = function (actualDate, formDate) {
                return $().add(text('Kontierst du in der falschen Woche?'))
                          .add(htmlElement('br'))
                          .add(text('(Es ist die Woche vom ' + getDateString(actualDate) + '.)'));
            }
        } else {
            var convertDate = function (date) {
                return date;
            }
            var generateWarning = function (actualDate, formDate) {
                return $().add(text('Kontierst du am falschen Tag?'))
                          .add(htmlElement('br'))
                          .add(text('(Heute ist der ' + getDateString(actualDate) +  '.)'));
            }
        }
        
        var getFormDate = function (){
            var formDateString = jQuery('#hf_date').val();
            var dateData = formDateString.split(".");
            return new Date(parseInt(dateData[2], 10), parseInt(dateData[1], 10) - 1, parseInt(dateData[0], 10));
        }
        
        var formDate = convertDate(getFormDate());

        var now = new Date();
        var currentDate = convertDate(new Date(now.getFullYear(), now.getMonth(), now.getDate()));
    
        if (formDate.getTime() != currentDate.getTime()){
            displayMessage(generateWarning(currentDate, formDate));
        }
    }

    insertSearchBar();
    $(FILTER_INPUT_SELECTOR).keyup(getFilterInvoker());
    $(FILTER_INPUT_SELECTOR).keydown(preventSubmit);
    $(ALL_COST_CENTER_SELECTOR).keydown(checkDate);
    $(document).keydown(focusSearchBar);
    checkDate();
      
  }
);