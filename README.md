# Release Notes

## 0.5
\#5: a warning is displayed if bookings are done on wrong day or in wrong week

additional changes:
- some refactoring

## 0.4
\#4: filtering by more than one term possible now:

- by adding another term the already filtered cost centers will be further narrowed down
- the order of the terms is not relevant
- the terms do not have to match complete words of a cost center title
- spaces are considered as separators
- only terms longer than 2 characters are considered
- additionally the old behavior is still working: cost centers which contain the exact term given (including spaces) are included in the filter results

additional changes:

- redesign of search bar: no more opacity, more in line with the layout of the page
-- maybe \#3 is obsolete now
- \#6: form will not be submitted anymore on "enter" in search bar


## 0.3

- text in search bar is now selected after ctrl+f
- \#2: fixed some issues with the reset of the form


## 0.2

- ctrl+f causes focus on search bar


## 0.1

- Chrome (Tampermonky) compatibility
- some refactoring

# License
Distributed under the Apache License, Version 2.0
